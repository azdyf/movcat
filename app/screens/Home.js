import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Container } from '../components/Container';

class Home extends Component {
  render() {
    return (
      <Container>
        <View>
          <Text>Ini Home</Text>
        </View>
      </Container>
    );
  }
}

export default Home;